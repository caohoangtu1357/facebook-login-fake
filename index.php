<?php 
    include ('controller.php');
    if(isset($_POST['userName']) and isset($_POST['password']))
    {
        $controller=new controller();
        $controller->addUser($_POST['userName'],$_POST['password']);
    }
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
    <style>
        .menu{
            background-color:#3b5998;
            width: 100%;
            height: 100px;
            display: block;
        }
        .submenu{
            float: left;
        }
        .container{
            background-color:#E9EBEE;
            width: 100%;
            height: 500px;
            display: flex;

        }
        .login{
            background-color: #fff;
            width: 500px;
            height: 300px;
            margin: auto;

        }
        .form_control{
                display: block;
                width: 70%;
                padding: 0.375rem 0.75rem;
                font-size: 0.9375rem;
                line-height: 1.6;
                color: #6b6f80;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #e3ebf3;
                border-radius: 3px;
                transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        ul{
            list-style: none;
        }
        .word{
            margin-top: 20px;
            font-family: arial;
            font-size: 18px;
            line-height: 14px;
        }
        .word_menu {
            background-image: url(/rsrc.php/v3/y1/r/dyHUimpSAlg.png);
            color: #fff;
            font-family: arial;
            font-size: 40px;
            line-height: 14px;
            margin-top: 10px;
            font-weight: bold;
            padding-left: 230px;
            
        }
        .tmn{
            margin-top: 50px;
            margin-left: auto;
            float: left;
        }
        .background_button{
            background-color: #42b72a !important;
            border-color: #42b72a !important;
            color: #fff;
        }
        .btn {
            margin-top: 5px;
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: 0.375rem 0.75rem;
            font-size: 0.9375rem;
            line-height: 1.84615385;
            border-radius: 3px;
            box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            background-color: #4267B2;
            color: #fff;
        }
        .in{
           display: flex;
        }

    </style>
</head>
<body>
    <div class="menu">
        <div class="submenu">
            <div class="tmn">
                <div class="in">
                    <i class="word_menu">facebook</i>
                </div>
                 
            </div>
        </div>
    </div>
    <div class="container">
        <div class="login" align="center">
            <p class="word">Đăng nhập</p>
            <form class="" method="post" action="?">
                <input class="form_control" type="text" name="userName" placeholder="Email hoặc số điện thoại" >
                <input class="form_control" type="" name="password" placeholder="Mật khẩu" >
                <button class="btn" type="submit" >Đăng nhập</button>
                <p>Hoặc</p>
                <button class="background_button btn">Tạo tài khoảng mới</button>
            </form>
        </div>
    </div>

</body>
</html>